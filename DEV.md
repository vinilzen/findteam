FindTeam
========================

This is a service for find the team for games or other events.

How to start?
--------------

Clone repository

`git clone git@bitbucket.org:vinilzen/findteam.git`


Run Composer

`/usr/local/php70/bin/php composer.phar install`


Create DB and schema:

`php70 bin/console doctrine:database:create` 
`php70 bin/console doctrine:schema:create`


Validate:

`php70 bin/console doctrine:schema:validate`


Load Data Fixture:

`php70 bin/console doctrine:fixtures:load`


Set pass:

`php70 bin/console fos:user:change-password <username> <password>`


