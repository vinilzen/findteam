<?php

namespace AppBundle\Controller;

use Doctrine\ORM\Query;
use Doctrine\ORM\Query\ResultSetMapping;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class DefaultController extends Controller
{
	/**
	 * @Route("/", name="homepage")
	 */
	public function indexAction(Request $request)
	{
		$em = $this->get('doctrine')->getManager();
		$repository = $em->getRepository('AppBundle:Game');

		$countryCode = '';
		if ( $request->query->has('_country_code')) {
			$countryCode = $request->query->get('_country_code');
		} else if ($request->cookies->has('_country_code')) {
			$countryCode = $request->cookies->get('_country_code');
		}

		$query = $repository->createQueryBuilder('g')
			->where('g.country = :countryCode')
			->andWhere('g.event_date > :yesterday')
			->setParameters(array(
				'countryCode' => $countryCode,
				'yesterday' => new \DateTime('yesterday 23:59:59')
			))
			->orderBy('g.event_date', 'DESC')
			->getQuery();

		$events = $query->getResult();

		// get only unique countries from exist events
		$query = $repository->createQueryBuilder('g')
			->select('DISTINCT g.country')->getQuery();
		$uniqCountries = $query->getResult();
		$countryCodes = [];

		foreach ($uniqCountries as $k => $v) {
			$countryCodes[] = $v['country'];
		}

		// replace this example code with whatever you need
		return $this->render('default/index.html.twig', [
			'events' => $events,
			'countryCodes' => $countryCodes
		]);
	}


	/**
	 * @Route("/send", name="send")
	 */
	public function sendAction(\Swift_Mailer $mailer)
	{
		$message = (new \Swift_Message('Hello Email'))
			->setFrom('info@findteam.xyz')
			->setTo('marchukilya@gmail.com')
			->setBody(
				// $this->renderView(
				// app/Resources/views/Emails/registration.html.twig
				// 'Emails/registration.html.twig',
				// array('name' => $name)
				// ),
				'Hi guys! How are you? Your <strong>IL</strong>',
				'text/html'
			);
		$mailer->send($message);

		return $this->render('default/message.html.twig', [
			'message' => 'Sended'
		]);
	}

}
