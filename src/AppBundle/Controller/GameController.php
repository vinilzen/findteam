<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Application;
use AppBundle\Entity\Game;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\User\UserInterface;

/**
 * Game controller.
 *
 * @Route("game")
 */
class GameController extends Controller
{
	/**
	 * Find Event
	 *
	 * @Route("/find", name="find_game")
	 * @Method({"GET", "POST"})
	 */
	public function findEventAction() {

		//$form = $this->createForm('AppBundle\Form\GameType', $game);
		//$form->handleRequest($request);


		return $this->render('game/find.html.twig', array(

		));
	}


	/**
	 * Creates a new game entity.
	 *
	 * @Security("has_role('ROLE_USER')")
	 * @Route("/new", name="game_new")
	 * @Method({"GET", "POST"})
	 */
	public function newAction(Request $request, UserInterface $user = null)
	{
		$game = new Game();
		$game->setAuthor($user);
		$form = $this->createForm('AppBundle\Form\GameType', $game);
		$form->handleRequest($request);

		if ($form->isSubmitted() && $form->isValid()) {
			$em = $this->getDoctrine()->getManager();
			$em->persist($game);
			$em->flush($game);

			return $this->redirectToRoute('game_main', array('id' => $game->getId()));
		}

		return $this->render('game/new.html.twig', array(
			'game' => $game,
			'form' => $form->createView(),
		));
	}

	/**
	 * Show game details
	 *
	 * @Route("/{id}", name="game_main")
	 *
	 * @Method("GET")
	 */
	public function mainAction(Game $game, UserInterface $user = null)
	{
		$deleteGameForm = $this->createDeleteForm($game);

		$application = $this->getDoctrine()
			->getRepository('AppBundle:Application')
			->findOneBy(array('user' => $user, 'game' => $game));

		if (!$application) {

			$application = new Application();

			$form = $this->createForm('AppBundle\Form\ApplicationType', $application, array(
				'action' => $this->generateUrl('application_new')
			));

			$form->get('game')->setData($game);
			$form->get('user')->setData($user);

		} else {

			$deleteApplicationForm = $this->createFormBuilder()
				->setAction($this->generateUrl('application_delete', array('id' => $application->getId())))
				->setMethod('DELETE')
				->getForm();
		}

		return $this->render('game/main.html.twig', array(
			'event' => $game,
			'user' => $user,
			'form' => isset($form) ? $form->createView() : '',
			'delete_game_form' => $deleteGameForm->createView(),
			'delete_app_form' => isset($deleteApplicationForm) ? $deleteApplicationForm->createView() : '',
		));
	}

	/**
	 * Lists all game entities.
	 *
	 * @Route("/", name="game_index")
	 * @Method("GET")
	 */
	public function indexAction(Request $request)
	{
		$em = $this->getDoctrine()->getManager();
		$repository= $em->getRepository('AppBundle:Game');

		$countryCode = '';
		if ( $request->query->has('_country_code')) {
			$countryCode = $request->query->get('_country_code');
		} else if ($request->cookies->has('_country_code')) {
			$countryCode = $request->cookies->get('_country_code');
		}

		$query = $repository->createQueryBuilder('g')
			->where('g.country = :countryCode')
			->andWhere('g.event_date > :yesterday')
			->setParameters(array(
				'countryCode' => $countryCode,
				'yesterday' => new \DateTime('yesterday 23:59:59')
			))
			->orderBy('g.event_date', 'DESC')
			->getQuery();

		$games = $query->getResult();

		$query = $repository->createQueryBuilder('g')
			->select('DISTINCT g.country')->getQuery();
		
		$uniqCountries = $query->getResult();
		$countryCodes = [];

		foreach ($uniqCountries as $k => $v) {
			$countryCodes[] = $v['country'];
		}

		return $this->render('game/index.html.twig', array(
			'games' => $games,
			'countryCodes' => $countryCodes
		));
	}

	/**
	 * Displays a form to edit an existing game entity.
	 *
	 * @Route("/{id}/edit", name="game_edit")
	 * @Method({"GET", "POST"})
	 */
	public function editAction(Request $request, Game $game)
	{
		$deleteForm = $this->createDeleteForm($game);
		$editForm = $this->createForm('AppBundle\Form\GameType', $game);
		$editForm->handleRequest($request);

		if ($editForm->isSubmitted() && $editForm->isValid()) {
			$this->getDoctrine()->getManager()->flush();

			return $this->redirectToRoute('game_edit', array('id' => $game->getId()));
		}

		return $this->render('game/edit.html.twig', array(
			'game' => $game,
			'edit_form' => $editForm->createView(),
			'delete_form' => $deleteForm->createView(),
		));
	}

	/**
	 * Deletes a game entity.
	 *
	 * @Route("/{id}", name="game_delete")
	 * @Method("DELETE")
	 */
	public function deleteAction(Request $request, Game $game)
	{
		$form = $this->createDeleteForm($game);
		$form->handleRequest($request);

		if ($form->isSubmitted() && $form->isValid()) {
			$em = $this->getDoctrine()->getManager();
			$em->remove($game);
			$em->flush();
		}

		return $this->redirectToRoute('game_index');
	}

	/**
	 * Creates a form to delete a game entity.
	 *
	 * @param Game $game The game entity
	 *
	 * @return \Symfony\Component\Form\Form The form
	 */
	private function createDeleteForm(Game $game)
	{
		return $this->createFormBuilder()
			->setAction($this->generateUrl('game_delete', array('id' => $game->getId())))
			->setMethod('DELETE')
			->getForm()
		;
	}
}
