<?php

/*
 * This file is part of the Symfony package.
 *
 * (c) Fabien Potencier <fabien@symfony.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace AppBundle\Twig;

use Symfony\Component\Intl\Intl;
use Symfony\Component\HttpFoundation\RequestStack;

/**
 * This Twig extension adds a new 'md2html' filter to easily transform Markdown
 * contents into HTML contents inside Twig templates.
 *
 * See http://symfony.com/doc/current/cookbook/templating/twig_extension.html
 *
 * In addition to creating the Twig extension class, before using it you must also
 * register it as a service. See app/config/services.yml file for details.
 *
 * @author Ryan Weaver <weaverryan@gmail.com>
 * @author Javier Eguiluz <javier.eguiluz@gmail.com>
 * @author Julien ITARD <julienitard@gmail.com>
 */
class AppExtension extends \Twig_Extension
{
    private $locales;

    private $requestStack;

    public function __construct(string $locales, RequestStack $requestStack)
    {
        $this->locales = $locales;
	    $this->requestStack = $requestStack;
    }

    /**
     * {@inheritdoc}
     */
    public function getFunctions()
    {
        return [
            new \Twig_SimpleFunction('locales', [$this, 'getLocales']),
            new \Twig_SimpleFunction('country', [$this, 'getSelectedRegion']),
            new \Twig_SimpleFunction('countries', [$this, 'getCountries']),
            new \Twig_SimpleFunction('user_applied', [$this, 'checkUserApply']),
        ];
    }


    public function getFilters()
    {
        return array(
            new \Twig_SimpleFilter('countryName', array($this, 'countryName')),
        );
    }

    public function countryName($countryCode){
        return Intl::getRegionBundle()->getCountryName($countryCode);
    }

    public function checkUserApply($user, $applications)
    {
        $applied = false;
        foreach ($applications as $application) {
            if ($application->getUser()->getId() == $user->getId()) {
                $applied = true;
                break;
            }
        }

        return $applied;
    }

    public function getSelectedRegion()
    {
	    $request = $this->requestStack->getCurrentRequest();

	    $countryName = false;

	    if ( $request->query->has('_country_name')) {
	        $countryName = $request->query->get('_country_name');
	    } else if ($request->cookies->has('_country_name')) {
		    $countryName = $request->cookies->get('_country_name');
	    }

	    return $countryName;
    }

	public function getCountries()
	{
		$countryNames = Intl::getRegionBundle()->getCountryNames();

		$countries = [];
		foreach ($countryNames as $countryCode => $countryName) {
			$countries[] = [
				'code' => $countryCode,
				'name' => $countryName
			];
		}

		return $countries;
	}

    /**
     * Takes the list of codes of the locales (languages) enabled in the
     * application and returns an array with the name of each locale written
     * in its own language (e.g. English, Français, Español, etc.).
     *
     * @return array
     */
    public function getLocales()
    {
        $localeCodes = explode('|', $this->locales);

        $locales = [];
        foreach ($localeCodes as $localeCode) {
            $locales[] = [
            	'code' => $localeCode,
	            'name' => Intl::getLocaleBundle()->getLocaleName($localeCode, $localeCode)
            ];
        }

        return $locales;
    }

    /**
     * {@inheritdoc}
     */
    public function getName()
    {
        // the name of the Twig extension must be unique in the application. Consider
        // using 'app.extension' if you only have one Twig extension in your application.
        return 'app.extension';
    }
}
