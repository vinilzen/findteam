<?php


namespace AppBundle\EventSubscriber;

use Symfony\Component\HttpKernel\Event\GetResponseEvent;
use Symfony\Component\HttpKernel\KernelEvents;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpFoundation\Cookie;
use Symfony\Component\HttpKernel\Event\FilterResponseEvent;

/**
 * Class RegionSubscriber
 */
class RegionSubscriber implements EventSubscriberInterface
{
	public function onKernelResponse(FilterResponseEvent $event)
	{
		$request = $event->getRequest();

		if ( $request->query->has('_country_name') && $request->query->has('_country_code') )
		{
			$countryName = $request->query->get('_country_name');
			$countryCode = $request->query->get('_country_code');

			$response = $event->getResponse();
			$time = time() + (3600 * 24 * 180);
			$response->headers->setCookie(new Cookie('_country_code', $countryCode, $time));
			$response->headers->setCookie(new Cookie('_country_name', $countryName, $time));
		}
	}

	public static function getSubscribedEvents()
	{
		return array(
			KernelEvents::RESPONSE => 'onKernelResponse'
		);
	}
}