<?php


namespace AppBundle\EventSubscriber;

use Symfony\Component\HttpKernel\Event\GetResponseEvent;
use Symfony\Component\HttpKernel\KernelEvents;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;


/**
 * Class LocaleListener
 */
class LocaleSubscriber implements EventSubscriberInterface
{
	/**
	 * List of supported locales.
	 *
	 * @var array
	 */
	private $locales;

	/**
	* @var string
	*/
	private $defaultLocale;

	public function __construct(string $defaultLocale, string $locales)
	{
		$this->defaultLocale = $defaultLocale;
		$this->locales = explode('|', trim($locales));
	}

	public function onKernelRequest(GetResponseEvent $event)
	{
		$request = $event->getRequest();

		if ($locale = $request->attributes->get('_locale')) {

			$request->getSession()->set( '_locale', $locale );
			$request->setLocale($locale);

		} else if ($locale = $request->query->get('_locale')) {

			$request->getSession()->set( '_locale', $locale );
			$request->setLocale($locale);

		} else {

			if (!$request->hasPreviousSession()) {
				$preferredLanguage = $request->getPreferredLanguage($this->locales);
				$request->setLocale($preferredLanguage);
			} else {
				// if no explicit locale has been set on this request, use one from the session
				$request->setLocale($request->getSession()->get('_locale', $this->defaultLocale));
			}
		}
	}

	public static function getSubscribedEvents()
	{
		return array(
			// must be registered after the default Locale listener
			KernelEvents::REQUEST => array(array('onKernelRequest', 15)),
		);
	}
}
