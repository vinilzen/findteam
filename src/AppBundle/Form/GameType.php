<?php

namespace AppBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\CountryType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\TimeType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Validator\Constraints\Type;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;


class GameType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
	        ->add('country', CountryType::class)
	        ->add('city')
	        ->add('address')
	        ->add('event_date', DateType::class, array(
		        'constraints' => array(
			        new NotBlank(),
			        new Type('\DateTime')
		        ),
		        'widget' => 'single_text',
		        'html5' => false,
		        'placeholder' => 'YYYY-MM-DD',
		        'attr' => ['class' => 'js-datepicker'],
	        ))
	        ->add('time_start', TimeType::class, array(
		        'constraints' => array(
			        new NotBlank(),
			        new Type('\DateTime')
		        ),
		        'widget' => 'single_text',
		        'html5' => false,
		        'attr' => ['class' => 'js-timepicker',
		                   'data-provide' => 'timepicker',
		                   'data-show-meridian' => 'false',
		                   'data-minute-step' => 5,
		        ],
	        ))
	        ->add('time_end', TimeType::class, array(
		        'constraints' => array(
			        new NotBlank(),
			        new Type('\DateTime')
		        ),
		        'widget' => 'single_text',
		        'html5' => false,
		        'attr' => ['class' => 'js-timepicker',
		                   'data-provide' => 'timepicker',
		                   'data-show-meridian' => 'false',
		                   'data-minute-step' => 5,
		        ],
	        ))
	        ->add('description')
	        ->add('quantity')
	        ->add('price')
	        ->add('level')
	        ->add('sex', ChoiceType::class, array(
	        	'choices' => array(
	        		'All' => 'A',
	        		'Men' => 'M',
	        		'Women' => 'W'
	        	)
	        ))
	        ->add('contact')
	        ->add('kind', EntityType::class, array(
		        'class' => 'AppBundle:Kind',
		        'required' => true,
		        'choice_translation_domain' => 'messages'
	        ));
    }
    
    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'AppBundle\Entity\Game'
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'appbundle_game';
    }


}
