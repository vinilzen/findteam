<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * Game
 *
 * @ORM\Table(name="game")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\GameRepository")
 */
class Game
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="country", type="string")
     */
    private $country;

    /**
     * @var string
     *
     * @ORM\Column(name="city", type="string")
     */
    private $city;

    /**
     * @var string
     *
     * @ORM\Column(name="address", type="string")
     */
    private $address;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="event_date", type="date")
     */
    private $event_date;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="time_start", type="time")
     */
    private $time_start;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="time_end", type="time")
     */
    private $time_end;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="text")
     */
    private $description;

    /**
     * @var integer
     *
     * @ORM\Column(name="quantity", type="integer")
     */
    private $quantity;

    /**
     * @var int
     *
     * @ORM\Column(name="price", type="integer")
     */
    private $price = 0;

    /**
     * @var int
     *
     * @ORM\Column(name="level", type="integer")
     */
    private $level = 0;

    /**
     * @var string
     *
     * @ORM\Column(name="sex", type="string", length=1)
     */
    private $sex = 'A';

    /**
     * @var string
     *
     * @ORM\Column(name="contact", type="string", nullable=true)
     */
    private $contact;

	/**
	 * @var bool
	 *
	 * @ORM\Column(name="enabled", type="boolean")
	 */
	private $enabled = true;

	/**
	 * @var \DateTime
	 *
	 * @ORM\Column(name="creation_time", type="datetime")
	 */
	private $creationTime;

	/**
	 * @var \AppBundle\Entity\Kind
	 *
	 * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Kind")
	 * @ORM\JoinColumns({
	 *   @ORM\JoinColumn(name="kind_id", referencedColumnName="id")
	 * })
	 */
	private $kind;

	/**
	 * @var \AppBundle\Entity\User
	 *
	 * @ORM\ManyToOne(targetEntity="AppBundle\Entity\User")
	 * @ORM\JoinColumns({
	 *   @ORM\JoinColumn(name="user_id", referencedColumnName="id")
	 * })
	 */
	private $author;

	/**
	 * @var \Doctrine\Common\Collections\Collection
	 *
	 * @ORM\OneToMany(targetEntity="AppBundle\Entity\Application", mappedBy="game")
	 */
	private $applications;

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set country
     *
     * @param string $country
     * @return Game
     */
    public function setCountry($country)
    {
        $this->country = $country;

        return $this;
    }

    /**
     * Get country
     *
     * @return string 
     */
    public function getCountry()
    {
        return $this->country;
    }

    /**
     * Set city
     *
     * @param string $city
     * @return Game
     */
    public function setCity($city)
    {
        $this->city = $city;

        return $this;
    }

    /**
     * Get city
     *
     * @return string 
     */
    public function getCity()
    {
        return $this->city;
    }

    /**
     * Set address
     *
     * @param string $address
     * @return Game
     */
    public function setAddress($address)
    {
        $this->address = $address;

        return $this;
    }

    /**
     * Get address
     *
     * @return string 
     */
    public function getAddress()
    {
        return $this->address;
    }

    /**
     * Set event_date
     *
     * @param \DateTime $eventDate
     * @return Game
     */
    public function setEventDate($eventDate)
    {
        $this->event_date = $eventDate;

        return $this;
    }

    /**
     * Get event_date
     *
     * @return \DateTime 
     */
    public function getEventDate()
    {
        return $this->event_date;
    }

    /**
     * Set time_start
     *
     * @param \DateTime $timeStart
     * @return Game
     */
    public function setTimeStart($timeStart)
    {
        $this->time_start = $timeStart;

        return $this;
    }

    /**
     * Get time_start
     *
     * @return \DateTime 
     */
    public function getTimeStart()
    {
        return $this->time_start;
    }

    /**
     * Set time_end
     *
     * @param \DateTime $timeEnd
     * @return Game
     */
    public function setTimeEnd($timeEnd)
    {
        $this->time_end = $timeEnd;

        return $this;
    }

    /**
     * Get time_end
     *
     * @return \DateTime 
     */
    public function getTimeEnd()
    {
        return $this->time_end;
    }

    /**
     * Set description
     *
     * @param string $description
     * @return Game
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string 
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set quantity
     *
     * @param integer $quantity
     * @return Game
     */
    public function setQuantity($quantity)
    {
        $this->quantity = $quantity;

        return $this;
    }

    /**
     * Get quantity
     *
     * @return \integer
     */
    public function getQuantity()
    {
        return $this->quantity;
    }

    /**
     * Set price
     *
     * @param integer $price
     * @return Game
     */
    public function setPrice($price)
    {
        $this->price = $price;

        return $this;
    }

    /**
     * Get price
     *
     * @return integer 
     */
    public function getPrice()
    {
        return $this->price;
    }

    /**
     * Set level
     *
     * @param integer $level
     * @return Game
     */
    public function setLevel($level)
    {
        $this->level = $level;

        return $this;
    }

    /**
     * Get level
     *
     * @return integer 
     */
    public function getLevel()
    {
        return $this->level;
    }

    /**
     * Set sex
     *
     * @param string $sex
     * @return Game
     */
    public function setSex($sex)
    {
        $this->sex = $sex;

        return $this;
    }

    /**
     * Get sex
     *
     * @return string 
     */
    public function getSex()
    {
        return $this->sex;
    }

    /**
     * Set contact
     *
     * @param string $contact
     * @return Game
     */
    public function setContact($contact)
    {
        $this->contact = $contact;

        return $this;
    }

    /**
     * Get contact
     *
     * @return string 
     */
    public function getContact()
    {
        return $this->contact;
    }

	/**
	 * Set enabled
	 *
	 * @param boolean $enabled
	 * @return Game
	 */
	public function setEnabled($enabled)
	{
		$this->enabled = $enabled;

		return $this;
	}

	/**
	 * Get enabled
	 *
	 * @return boolean
	 */
	public function getEnabled()
	{
		return $this->enabled;
	}

	/**
	 * Set kind
	 *
	 * @param \AppBundle\Entity\Kind $kind
	 *
	 * @return Game
	 */
	public function setKind(Kind $kind = null)
	{
		$this->kind = $kind;

		return $this;
	}

	/**
	 * Get kind
	 *
	 * @return \AppBundle\Entity\Kind
	 */
	public function getKind()
	{
		return $this->kind;
	}

	public function __toString() {
		return $this->city.' '.$this->address.' '.$this->time_start->format('Y-m-d H:i:s');
	}

	/**
	 * Constructor
	 */
	public function __construct()
	{
		$this->applications = new ArrayCollection();
		$this->creationTime = new \DateTime();
		$this->setEventDate(new \DateTime('tomorrow'));
		$this->setTimeStart(new \DateTime('tomorrow 19:00'));
		$this->setTimeEnd(new \DateTime('tomorrow 20:30'));

	}

	/**
	 * Add application
	 *
	 * @param \AppBundle\Entity\Application $application
	 *
	 * @return Game
	 */
	public function addApplication(Application $application)
	{
		$this->applications[] = $application;

		return $this;
	}

	/**
	 * Remove application
	 *
	 * @param \AppBundle\Entity\Application $application
	 */
	public function removeApplication(Application $application)
	{
		$this->applications->removeElement($application);
	}

	/**
	 * Get applications
	 *
	 * @return \Doctrine\Common\Collections\Collection
	 */
	public function getApplications()
	{
		return $this->applications;
	}

	/**
	 * Set creationTime
	 *
	 * @param \DateTime $creationTime
	 *
	 * @return Game
	 */
	public function setCreationTime($creationTime)
	{
		$this->creationTime = $creationTime;

		return $this;
	}

	/**
	 * Get $this->creationTime
	 *
	 * @return \DateTime
	 */
	public function getCreationTime()
	{
		return $this->creationTime;
	}

    /**
     * Set author
     *
     * @param \AppBundle\Entity\User $author
     * @return Game
     */
    public function setAuthor(User $author = null)
    {
        $this->author = $author;

        return $this;
    }

    /**
     * Get author
     *
     * @return \AppBundle\Entity\User
     */
    public function getAuthor()
    {
        return $this->author;
    }
}
