<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\UniqueConstraint;

/**
 * Application
 *
 * @ORM\Table(name="application", uniqueConstraints={@UniqueConstraint(name="application", columns={"user_id", "game_id"})})
 * @ORM\Entity(repositoryClass="AppBundle\Repository\ApplicationRepository")
 */
class Application
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

	/**
	 * @var \AppBundle\Entity\User
	 *
	 * @ORM\ManyToOne(targetEntity="AppBundle\Entity\User")
	 * @ORM\JoinColumns({
	 *   @ORM\JoinColumn(name="user_id", referencedColumnName="id")
	 * })
	 */
    private $user;

	/**
	 * @var \AppBundle\Entity\Game
	 *
	 * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Game", inversedBy="applications")
	 * @ORM\JoinColumns({
	 *   @ORM\JoinColumn(name="game_id", referencedColumnName="id", onDelete="CASCADE")
	 * })
	 */
    private $game;

    /**
     * @var bool
     *
     * @ORM\Column(name="confirmed", type="boolean")
     */
    private $confirmed = true;

	/**
	 * @var \DateTime
	 *
	 * @ORM\Column(name="registration_time", type="datetime")
	 */
	private $registrationTime;


	public function __construct()
	{
		$this->registrationTime = new \DateTime();
	}

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set user
     *
     * @param integer $user
     * @return Application
     */
    public function setUser($user)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return User
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * Set game
     *
     * @param integer $game
     * @return Application
     */
    public function setGame($game)
    {
        $this->game = $game;

        return $this;
    }

    /**
     * Get game
     *
     * @return Game
     */
    public function getGame()
    {
        return $this->game;
    }

    /**
     * Set confirmed
     *
     * @param boolean $confirmed
     * @return Application
     */
    public function setConfirmed($confirmed)
    {
        $this->confirmed = $confirmed;

        return $this;
    }

    /**
     * Get confirmed
     *
     * @return boolean 
     */
    public function getConfirmed()
    {
        return $this->confirmed;
    }

	/**
	 * Set registrationTime
	 *
	 * @param \DateTime $registrationTime
	 *
	 * @return Application
	 */
	public function setRegistrationTime($registrationTime)
	{
		$this->registrationTime = $registrationTime;

		return $this;
	}

	/**
	 * Get registrationTime
	 *
	 * @return \DateTime
	 */
	public function getRegistrationTime()
	{
		return $this->registrationTime;
	}
}
