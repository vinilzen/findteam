<?php

namespace AppBundle\DataFixtures\ORM;

use AppBundle\Entity\Application;
use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Validator\Constraints\DateTime;

class LoadApplicationData extends AbstractFixture implements OrderedFixtureInterface, ContainerAwareInterface
{   
    /**
     * @var ContainerInterface
     */
    private $container;

    public function setContainer(ContainerInterface $container = null)
    {
        $this->container = $container;
    }

	public function getOrder() {
		return 3;
	}

	public function load(ObjectManager $manager) {

		$application = new Application();
		$application->setUser($this->getReference('Jack'));
		$application->setGame($this->getReference('eventMinsk'));

		$manager->persist($application);

		$application1 = new Application();
		$application1->setUser($this->getReference('Vasya'));
		$application1->setGame($this->getReference('eventMinsk'));

		$manager->persist($application1);

		$manager->flush();
	}
}