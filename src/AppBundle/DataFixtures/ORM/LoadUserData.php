<?php


namespace AppBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Doctrine\Common\DataFixtures\AbstractFixture;
use AppBundle\Entity\User;

class LoadUserData extends AbstractFixture implements FixtureInterface, ContainerAwareInterface
{
	/**
	 * @var ContainerInterface
	 */
	private $container;

	public function setContainer(ContainerInterface $container = null)
	{
		$this->container = $container;
	}

	public function getOrder() {
		return 0;
	}

	public function load(ObjectManager $manager) {

		$encoder = $this->container->get('security.password_encoder');

		$userAdmin = new User();
		$userAdmin->setUsername('admin');
		$userAdmin->setFirstName('Ilya');
		$userAdmin->setLastName('Marchuk');
		$userAdmin->setEmail('ilya@marchuk.eu');
		$userAdmin->setPassword('$2y$13$bobekw4rdag4ksc4s440keF1.V0x/3r7MRKQXnoI.R8DjJBalXKk2');
		$manager->persist($userAdmin);

		$userIvan = new User();
		$userIvan->setUsername('Ivan');
		$userIvan->setFirstName('Ivan');
		$userIvan->setLastName('Ivanovicz');
		$userIvan->setEmail('ivan@tut.by');
		$userIvan->setEnabled(true);
		$userIvan->setSalt(md5(uniqid()));
		$password = $encoder->encodePassword($userIvan, 'Ivan');
		$userIvan->setPassword($password);
		$this->addReference('Ivan', $userIvan);
		$manager->persist($userIvan);


		$userJack = new User();
		$userJack->setUsername('Jack');
		$userJack->setFirstName('Jack');
		$userJack->setLastName('Starch');
		$userJack->setEmail('jack@google.com');
		$userJack->setEnabled(true);
		$userJack->setSalt(md5(uniqid()));
		$password = $encoder->encodePassword($userJack, 'Jack');
		$userJack->setPassword($password);
		$this->addReference('Jack', $userJack);
		$manager->persist($userJack);

		$userVasya = new User();
		$userVasya->setUsername('Vasya');
		$userVasya->setFirstName('Vasya');
		$userVasya->setLastName('Pupkin');
		$userVasya->setEmail('vasya@ya.ru');
		$userVasya->setSalt(md5(uniqid()));
		$password = $encoder->encodePassword($userVasya, 'Vasya');
		$userVasya->setPassword($password);
		$userVasya->setEnabled(true);
		$this->addReference('Vasya', $userVasya);
		$manager->persist($userVasya);

		$userDaniel = new User();
		$userDaniel->setUsername('Daniel');
		$userDaniel->setFirstName('Daniel');
		$userDaniel->setLastName('Matiaz');
		$userDaniel->setEmail('daniel@barcelona.sp');
		$userDaniel->setSalt(md5(uniqid()));
		$password = $encoder->encodePassword($userDaniel, 'Daniel');
		$userDaniel->setPassword($password);
		$this->addReference('Daniel', $userDaniel);
		$manager->persist($userDaniel);

		$manager->flush();
	}
}