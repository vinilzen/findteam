<?php


namespace AppBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use AppBundle\Entity\Kind;

/**
 * Class LoadKindData
 */
class LoadKindData extends AbstractFixture implements OrderedFixtureInterface
{
	public function getOrder() {
		return 1;
	}

	public function load(ObjectManager $manager)
	{
		$kind1 = new Kind();
		$kind1->setName('Soccer');
		$kind1->setDescription('Association football, more commonly known as football or soccer, 
								is a team sport played between two teams of eleven players with 
								a spherical ball.');
		$kind1->setIcon('fa-futbol-o');

		$this->addReference('football', $kind1);

		$manager->persist($kind1);



		$kind2 = new Kind();
		$kind2->setName('Volleyball');
		$kind2->setDescription('Volleyball is a team sport in which two teams of six 
								players are separated by a net. Each team tries to score 
								points by grounding a ball on the other team\'s court 
								under organized rules.');
		$kind2->setIcon('fa-table');

		$manager->persist($kind2);



		$kind3 = new Kind();
		$kind3->setName('Poker');
		$kind3->setDescription('Poker is a family of card games that combine gambling, 
								strategy, and skill . All poker variants involve betting 
								as an intrinsic part of play, and determine the winner of 
								each hand according to the combinations of players\' cards, 
								at least some of which remain hidden until the end of the hand.');
		$kind3->setIcon('fa-heart');
		$this->addReference('poker', $kind3);

		$manager->persist($kind3);



		$kind4 = new Kind();
		$kind4->setName('Board Game');
		$kind4->setDescription('A board game is a tabletop game that involves counters 
								or pieces moved or placed on a pre-marked surface or "board", 
								according to a set of rules. Some games are based on pure 
								strategy, but many contain an element of chance; and some are 
								purely chance, with no element of skill.');
		$kind4->setIcon('fa-table');
		$this->addReference('board_game', $kind4);

		$manager->persist($kind4);

		$manager->flush();
	}

}