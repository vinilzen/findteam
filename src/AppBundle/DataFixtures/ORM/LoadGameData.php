<?php

namespace AppBundle\DataFixtures\ORM;

use AppBundle\Entity\Game;
use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Security\Acl\Domain\ObjectIdentity;
use Symfony\Component\Security\Acl\Domain\UserSecurityIdentity;
use Symfony\Component\Security\Acl\Permission\MaskBuilder;

/**
 * Class LoadGameData
 */
class LoadGameData extends AbstractFixture implements OrderedFixtureInterface, ContainerAwareInterface
{
    /**
     * @var ContainerInterface
     */
    private $container;

    public function setContainer(ContainerInterface $container = null)
    {
        $this->container = $container;
    }

	public function getOrder() {
		return 2;
	}

	public function load(ObjectManager $manager)
	{
		$event1 = new Game();
		$event1->setDescription('gramy od 10 osób');
		$event1->setAddress('Annopol 24B');
		$event1->setEventDate(new \DateTime('next saturday'));
		$event1->setTimeStart(new \DateTime('next saturday 19:00'));
		$event1->setTimeEnd(new \DateTime('next saturday 20:30'));
		$event1->setCity('Warszawa');
		$event1->setCountry('PL');
		$event1->setKind($this->getReference('football'));
		$event1->setQuantity(10);
		$event1->setAuthor($this->getReference('Jack'));
		$manager->persist($event1);


		/***********
		 * EVENT 2
		 ***********/
		$event2 = new Game();
		$event2->setDescription('Собираем 8 человек в зал на Машерова');
		$event2->setAddress('Машерова 10');
		$event2->setEventDate(new \DateTime('next saturday +1 week'));
		$event2->setTimeStart(new \DateTime('next saturday +1 week 19:00'));
		$event2->setTimeEnd(new \DateTime('next saturday +1 week 20:30'));
		$event2->setCity('Minsk');
		$event2->setCountry('BY');
		$event2->setKind($this->getReference('football'));
		$event2->setQuantity(8);
		$event2->setAuthor($this->getReference('Ivan'));
		$manager->persist($event2);
		$this->addReference('eventMinsk', $event2);
		$manager->flush();

		// creating the ACL
		$aclProvider = $this->container->get('security.acl.provider');
		$objectIdentity = ObjectIdentity::fromDomainObject($event2);
		$acl = $aclProvider->createAcl($objectIdentity);

		 // retrieving the user
		$user = $this->getReference('Ivan');
        $securityIdentity = UserSecurityIdentity::fromAccount($user);

 		// grant owner access
        $acl->insertObjectAce($securityIdentity, MaskBuilder::MASK_OWNER);
        $aclProvider->updateAcl($acl);
		/***********
		 * EVENT 2
		 ***********/



		/***********
		 * EVENT 3
		 ***********/
		$event3 = new Game();
		$event3->setDescription('Recogemos 12 personas en la sala');
		$event3->setAddress('Av. Dr. Marañón, 11 bajos');
		$event3->setEventDate(new \DateTime('next saturday +2 days'));
		$event3->setTimeStart(new \DateTime('next saturday +2 days 19:00'));
		$event3->setTimeEnd(new \DateTime('next saturday +2 days 20:30'));
		$event3->setCity('Barselona');
		$event3->setCountry('ES');
		$event3->setKind($this->getReference('football'));
		$event3->setQuantity(12);
		$event3->setAuthor($this->getReference('Daniel'));
		$manager->persist($event3);
		/***********
		 * EVENT 3
		 ***********/


		$event4 = new Game();
		$event4->setDescription('Texas hold \'em, 4 Players');
		$event4->setAddress('150-162 Edgware Road');
		$event4->setEventDate(new \DateTime('next saturday +3 days'));
		$event4->setTimeStart(new \DateTime('next saturday +3 days 19:00'));
		$event4->setTimeEnd(new \DateTime('next saturday +3 days 20:30'));
		$event4->setCity('London');
		$event4->setCountry('GB');
		$event4->setKind($this->getReference('poker'));
		$event4->setQuantity(12);
		$event4->setAuthor($this->getReference('Jack'));
		$manager->persist($event4);


		$event5 = new Game();
		$event5->setDescription('Texas hold \'em, 10 Players');
		$event5->setAddress('24 Basker Will');
		$event5->setEventDate(new \DateTime('next saturday +2 days'));
		$event5->setTimeStart(new \DateTime('next saturday +2 days 19:00'));
		$event5->setTimeEnd(new \DateTime('next saturday +2 days 20:30'));
		$event5->setCity('Manchester');
		$event5->setCountry('GB');
		$event5->setKind($this->getReference('poker'));
		$event5->setQuantity(12);
		$event5->setAuthor($this->getReference('Jack'));
		$manager->persist($event5);


		$event6 = new Game();
		$event6->setDescription('Pilka na hali');
		$event6->setAddress('Jana Pawla 24');
		$event6->setEventDate(new \DateTime('yesterday'));
		$event6->setTimeStart(new \DateTime('yesterday 19:00'));
		$event6->setTimeEnd(new \DateTime('yesterday 20:30'));
		$event6->setCity('Łodz');
		$event6->setCountry('PL');
		$event6->setKind($this->getReference('football'));
		$event6->setQuantity(6);
		$event6->setAuthor($this->getReference('Ivan'));
		$manager->persist($event6);

		$manager->flush();
	}

}